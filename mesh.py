import pyglet
from pyglet import image
import numpy, itertools
import primitive, group

class Sky(object):
    def __init__(self, camera, textures = None):
        self.batch = pyglet.graphics.Batch()
        self.camera = camera
        if not textures:
            textures = ['images/debug_positive_y.png',
                        'images/debug_negative_y.png',
                        'images/debug_positive_z.png',
                        'images/debug_negative_z.png',
                        'images/debug_positive_x.png',
                        'images/debug_negative_x.png']
        self.textures = dict(zip(primitive.cube_normals, 
                                 [pyglet.image.load(texture).get_texture() 
                                  for texture in textures]))

    def mesh(self):
        for key in primitive.cube_normals:
            verts = primitive.quad_vertices((-100,-100,-100), 
                                            key,
                                            (200,200,200))
            normals = key * 6
            tex_coords = primitive.tex_map((1,1), key)
            self.batch.add(len(verts)/3,
                           pyglet.gl.GL_TRIANGLES,
                           group.SkyTexture(self.textures[key], self.camera),
                           ('v3f', verts),
                           ('n3f', normals),
                           ('t2f', tex_coords))
        return self.batch

def print_timing(func):
    def wrapper(*arg):
        import time
        t1 = time.time()
        res = func(*arg)
        t2 = time.time()
        print '%s took %0.3f ms' % (func.func_name, (t2-t1)*1000.0)
        return res
    return wrapper

class Voxel(object):
    def __init__(self, source = None, neighbors = None, location = (0,0,0)):
        if source is not None:
            self.blocks = source
        else:
            self.blocks = numpy.array((16,16,16))
        self.location = location
        self.neighbor = neighbors
        self.x_slice = self.neighbor[(1,0,0)][0:1,:,:]
        self.z_slice = self.neighbor[(0,0,1)][:,:,0:1]
        self.nx_slice = self.neighbor[(-1,0,0)][-2:-1,:,:]
        self.nz_slice = self.neighbor[(0,0,-1)][:,:,-2:-1]
        self.z_slice = numpy.resize(self.z_slice, (18,128,1))
        self.nz_slice = numpy.resize(self.nz_slice, (18,128,1))

    def random(self, x, y, z):
        self.blocks = numpy.floor(3 * numpy.random.random((x,y,z))).astype(int)

    def filled(self, x, y, z): 
        self.blocks = numpy.ones((x,y,z)).astype(int)

    def erase(self, x, y, z, dx, dy, dz):
        self.blocks[x:x+dx,y:y+dy,z:z+dz] = numpy.zeros((dx,dy,dz))

    def mesh(self, texture_table):
        xoff = self.location[0] * 16
        yoff = self.location[1] * 16
        zoff = self.location[2] * 16
        
        merged_blocks = numpy.concatenate((self.nx_slice, self.blocks, self.x_slice), axis=0)
        merged_blocks = numpy.concatenate((self.nz_slice, merged_blocks, self.z_slice), axis=2)
        shape = self.blocks.shape

        # Build the mesh
        region_size = 4
        rs = region_size - 1
        region_range = list(itertools.product(*map(xrange, (1,1,1), (rs,rs,rs))))
        region_shape = (region_size,)*3

        def output_region(x,y,z, region):
            out = []
            if region.shape == region_shape:
                for rx,ry,rz in region_range:
                    v = region[rx,ry,rz]
                    if v != 0:
                        out.append((x+rx,y+ry,z+rz,v,1))
            return out

        def cull_geometry():
            output_list = [] # x,y,z,v
            for x,y,z in itertools.product(*map(xrange, (1,1,1), shape, (rs-1,rs-1,rs-1))):
                region = merged_blocks[x-1:x+rs,y-1:y+rs,z-1:z+rs]
                # Any empty blocks in this region
                if numpy.any(region == 0):
                    out = output_region(x,y,z,region)
                    if out:
                        output_list.extend(out)
            return output_list

        def calc_geometry(culled):
            tex_len = len(texture_table)
            verts, normals, tex_coords, colors = list(), list(), list(), list()
            for cube in culled:
                x, y, z, v, n = cube
                if v < tex_len and texture_table[v]:
                    primitive.inline_cube_vertices(verts, x+xoff, y+yoff, z+zoff, n)
                    normals += primitive.full_cube_normals
                    tex_map = zip(*[iter(primitive.cube_tex_map(n))]*2)
                    for n in range(len(tex_map)):
                        if type(texture_table[v]) == dict:
                            face = int(n/6)
                            table = texture_table[v][face]
                            if len(table) == 2:
                                tex_coords += (tex_map[n] + table)
                                colors += (1,1,1)
                            else:
                                tex_coords += (tex_map[n] + table[:2])
                                colors += table[2]
                        else:
                            tex_coords += (tex_map[n] + texture_table[v][:2])
                            colors += (1,1,1)
            return [verts,normals,tex_coords,colors]

        m = calc_geometry(cull_geometry())
        print("Rendered chunk {0}".format(self.location))

        # verts, normals, tex_coords, colors
        return m

