from pyglet.font import *

add_file('fonts/Orbitron/TTF/orbitron-black.ttf')
add_file('fonts/Orbitron/TTF/orbitron-medium.ttf')
add_file('fonts/Junction 02/Junction 02.ttf')

fonts = dict()
fonts['Orbitron'] = load('Orbitron')
fonts['Junction 02'] = load('Junction 02')
