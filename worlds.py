import numpy
from pyglet import image

import mesh, group
from camera import Camera

color_table = [(0,0,0,0), # Vacuum
               (128,128,128,255), # Rock
               (25,25,25,255), # Dark rock
               ]

texture_atlas = image.atlas.TextureAtlas()
textures = ['images/rock.png']
atlas = [texture_atlas.add(image.load(each)) for each in textures]
texture_group = group.Texture(texture_atlas.texture)
group_table = [None,
               group.Atlas(texture_group, 0.0, 0.0) # Rock
               ]

sky_textures = ['images/positive/y/nebula.jpg',
                'images/negative/y/nebula.jpg',
                'images/positive/z/nebula.jpg',
                'images/negative/z/nebula.jpg',
                'images/positive/x/nebula.jpg',
                'images/negative/x/nebula.jpg']

class GameWorld(object):
    chunkSize = (32,32,32)
    def __init__(self):
        self.level = None
        self.chunkPositions = []
        self.loaded = dict()
        self.camera = Camera(-6.0)
        self.sky = mesh.Sky(self.camera, sky_textures)
        self.meshes = {'sky':self.sky.mesh()}

    def getChunk(self, coord):
        if coord in self.chunkPositions:
            return self.level.getChunk(*coord)
        else:
            return None

    def load(self, chunk):
        if chunk not in self.loaded:
            chunkObject = self.getChunk(chunk)
            if chunkObject:
                self.loaded[chunk] = chunkObject.Blocks
            else:
                # Bad hardcoded values, fix later.
                self.loaded[chunk] = numpy.zeros(self.chunkSize)
        return self.loaded[chunk]

    def neighbor(self, chunk, direction):
        loc = map(lambda n,m: n+m, chunk, direction)
        return self.load((loc[0], loc[2]))

    def mesh_load(self, coord = (3,3), distance = 4):
        for chunk in self.chunkPositions:
            # Center loading around coord
            if not self.meshes.has_key(chunk) and \
                    math.sqrt((coord[0] - chunk[0])**2 + (coord[1] - chunk[1])**2) < distance:
                
                print("Rendering chunk... {0}".format(chunk))
                vm = mesh.Voxel(self.load(chunk), self, (chunk[0],0,chunk[1]))
                vm.color_table = color_table
                vm.group_table = group_table
                self.meshes[chunk] = vm.mesh()

    def generate(self):
        voxel = mesh.Voxel(world=self)
        voxel.color_table = color_table
        voxel.group_table = group_table
        voxel.random(20, 2, 10)
        #voxel.erase(5,5,0,10,10,30)
        self.meshes[(0,0,0)] = voxel.mesh()
