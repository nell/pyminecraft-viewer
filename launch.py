#!/usr/bin/env python
import pyglet
from pyglet.gl import *
from menu import menu

#pyglet.options['debug_gl'] = False  # Performance boost

def window_setup():
    window = pyglet.window.Window(width=1280,height=720,resizable=True)
    window.set_exclusive_mouse(False)
    window.exclusive = False

    glClearDepth(1.0)    
    glEnable(GL_DEPTH_TEST)
    #glDepthFunc(GL_LEQUAL)

    glAlphaFunc(GL_GREATER, 0.1);
    glEnable(GL_ALPHA_TEST);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glEnable(GL_BLEND)

    glEnable(GL_CULL_FACE) 

    glClearColor(0, 0, 0, 1)
    glEnableClientState(GL_VERTEX_ARRAY) # vbo

    @window.event
    def on_resize(width, height):
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(65, width / float(height), 1, 1000)
        glMatrixMode(GL_MODELVIEW)
        return pyglet.event.EVENT_HANDLED

    return window

window = window_setup()
menu(window)

pyglet.app.run()
