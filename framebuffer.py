import ctypes
from math import ceil, log
from pyglet.gl import *
from pyglet import image

if not gl_info.have_extension('GL_EXT_framebuffer_object'):
    raise RuntimeError("OpenGL does not support framebuffer objects.")

def poweroftwo(x):
    # Nearest power of two greater than x
    return int(2 ** ceil(log(x)/log(2)))

class Texture(object):
    """Work around for texture setup in pyglet."""
    def __init__(self, w, h):
        self.dtex = image.Texture.create(w, h)
        self.id = self.dtex.id
        self.target = self.dtex.target
        glBindTexture(self.target, self.dtex.id)
        glTexParameteri(self.target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glTexParameteri(self.target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
        glTexParameteri(self.target, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(self.target, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameteri(self.target, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY)
        glTexImage2D(self.target, 0, 
                     GL_RGBA, 
                     w, h, 
                     0, 
                     GL_RGBA, 
                     GL_UNSIGNED_BYTE, None)
        glBindTexture(self.target, 0)

    def get_region(self, *args):
        return self.dtex.get_region(*args)

class DepthTexture(Texture):
    def __init__(self, w, h):
        self.dtex = image.DepthTexture.create(w, h)
        self.id = self.dtex.id
        self.target = self.dtex.target
        glBindTexture(self.target, self.dtex.id)
        glTexParameteri(self.target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glTexParameteri(self.target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
        glTexParameteri(self.target, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(self.target, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameteri(self.target, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY)
        glTexImage2D(self.target, 0, 
                     GL_DEPTH_COMPONENT24, 
                     w, h, 
                     0, 
                     GL_DEPTH_COMPONENT, 
                     GL_UNSIGNED_BYTE, None)
        glBindTexture(self.target, 0)

class Framebuffer(object):
    def __init__(self, w, h):
        # Pyglet does this for textures, but not renderbuffers
        print("Bound {0} shape fb.".format((w,h)))

        # Create textures
        self.tex = Texture(w,h)
        self.dtex = DepthTexture(w,h)

        # Create renderbuffer
        """
        self.rbo = GLuint()
        glGenRenderbuffersEXT(1, ctypes.byref(self.rbo))
        glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, self.rbo)
        glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, 
                                 GL_DEPTH_COMPONENT, 
                                 w, h)
        glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0)"""

        # Create a frame buffer context
        self.fbo = GLuint()
        glGenFramebuffersEXT(1, ctypes.byref(self.fbo))
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, self.fbo)

        # Attach color texture
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, 
                                  GL_COLOR_ATTACHMENT0_EXT,
                                  self.tex.target, self.tex.id, 0)

        # Attach depth texture
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, 
                                  GL_DEPTH_ATTACHMENT_EXT,
                                  self.dtex.target, self.dtex.id, 0)

        # Attach renderbuffer
        """
        glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, 
                                     GL_DEPTH_ATTACHMENT_EXT, 
                                     GL_RENDERBUFFER_EXT, 
                                     self.rbo)"""

        status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT)
        if status != GL_FRAMEBUFFER_COMPLETE_EXT:
            raise RuntimeError("Could not create GL framebuffer", status)

        # Unbind
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)

    def bind(self):
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, self.fbo)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    def unbind(self):
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)

    def __del__(self):
        glDeleteFramebuffersEXT(1, ctypes.byref(self.fbo))
