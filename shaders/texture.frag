uniform sampler2D texture;

varying vec2 uv_coords;
varying vec2 atlas;

void main()
{
  vec2 mod_coords = mod(uv_coords, 0.0625);
  mod_coords += atlas;

  vec4 tex_color = texture2D(texture, mod_coords);

  gl_FragColor = tex_color * gl_Color;
}
