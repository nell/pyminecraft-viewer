// Texture
varying vec2 uv_coords;
varying vec2 atlas;

void main()
{	
  uv_coords = gl_MultiTexCoord0.st * 0.0625;
  atlas = gl_MultiTexCoord0.pq;

  gl_FrontColor = gl_Color;
  gl_Position = ftransform();
} 
