varying vec2 texCoord;

void main(void) {
	 gl_Position = ftransform();
	 texCoord = gl_Position.xy / 2.0 + 0.5;
	 gl_FrontColor = gl_Color;
}
