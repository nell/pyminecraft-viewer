import pyglet
from pyglet.gl import *
from pyglet.window import key
from pyglet.window import mouse
from ctypes import pointer, sizeof
from math import sin, cos

import shaders, minecraft, worlds, framebuffer

def event_setup(window, camera):
    window.debug = False

    @window.event
    def on_key_press(symbol, modifiers):
        move_speed = 0.5
        if symbol == key.W:
            camera.dz = move_speed
        elif symbol == key.S:
            camera.dz = -move_speed
        elif symbol == key.D:
            camera.dx = -move_speed
        elif symbol == key.A:
            camera.dx = move_speed
        elif symbol == key.Z:
            camera.dy = -move_speed
        elif symbol == key.X:
            camera.dy = move_speed
        elif symbol == key.BACKSPACE:
            window.set_exclusive_mouse(False)
            window.exclusive = False
        elif symbol == key.F10:
            window.debug = not window.debug
        elif symbol == key.F11:
            camera.fly = not camera.fly
        elif symbol == key.F12:
            camera.SSAO = not camera.SSAO


    @window.event
    def on_key_release(symbol, modifiers):
        if symbol in (key.S, key.W):
            camera.dz = 0
        elif symbol in (key.D, key.A):
            camera.dx = 0
        elif symbol in (key.X, key.Z):
            camera.dy = 0

    @window.event
    def on_mouse_motion(x, y, dx, dy):
        # Rotate the camera by dx, dy
        if window.exclusive:
            camera.aim_camera(dx, dy)

    @window.event
    def on_mouse_press(x, y, button, modifiers):
        window.set_exclusive_mouse(True)
        window.exclusive = True

def text_draw(window, camera):
    camera.orthographic_projection(window)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    window.fps_display.draw()

def viewer(window):
    print("Loading minecraft world 1...")
    world = minecraft.MinecraftWorld(1)
    camera = world.camera
    print("Done.")

    camera.SSAO = True

    fps_display = pyglet.clock.ClockDisplay()

    pos = world.level.getPlayerPosition()
    camera.x = pos[0]
    camera.y = pos[1]
    camera.z = pos[2]

    world.mesh_load((camera.x/16, camera.z/16))
    world.mesh_load((camera.x/16, camera.z/16))

    camera.aim_camera(0, 0)

    window.set_vsync(False)

    scene_fb = framebuffer.Framebuffer(window.width,window.height)

    def chunk_load(dt):
        world.mesh_load((camera.x/16, camera.z/16))

    @window.event
    def on_draw():
        if window.debug:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        camera.perspective_projection(window)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        gluLookAt(camera.x, camera.y, camera.z,
                  camera.x + camera.rx,
                  camera.y + camera.ry,
                  camera.z + camera.rz,
                  0.0, 1.0, 0.0)

        # Main rendering
        shaders.texture.bind()
        if camera.SSAO:
            scene_fb.bind() # SSAO prep
        glEnable(GL_TEXTURE_2D)
        glBindTexture(GL_TEXTURE_2D, minecraft.texture_atlas.id)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        world.draw()
        if camera.SSAO:
            scene_fb.unbind() # SSAO prep
        shaders.texture.unbind()
        glDisable(GL_TEXTURE_2D)

        if window.debug:
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

        #scene_fb.tex.get_region(0,0,window.width/2,window.height).blit(0,0)
        text_draw(window, camera)

        # SSAO
        if camera.SSAO:
            shaders.ssao.bind()
            glEnable(scene_fb.tex.target)
            glActiveTexture(GL_TEXTURE1)
            glBindTexture(scene_fb.tex.target, scene_fb.tex.id)
            glActiveTexture(GL_TEXTURE0)
            glBindTexture(scene_fb.dtex.target, scene_fb.dtex.id)
            shaders.ssao.uniformi("depth_texture", 0)
            shaders.ssao.uniformi("scene_texture", 1)
            shaders.ssao.uniformf("camerarange", 0.1, 1000)
            shaders.ssao.uniformf("screensize", window.width, window.height)

            glRectf(0,0,window.width,window.height)

            glDisable(scene_fb.tex.target)
            shaders.ssao.unbind()


    event_setup(window, camera)
    pyglet.clock.schedule_interval(camera.camera_update, 1/30.0)
    pyglet.clock.schedule_interval(chunk_load, 1)


def game(window):
    world = worlds.GameWorld()
    world.generate()
    camera = world.camera

    camera.aim_camera(0, 0)

    @window.event
    def on_draw():
        if window.debug:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        camera.perspective_projection(window)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        gluLookAt(camera.x, camera.y, camera.z,
                  camera.x + camera.rx,
                  camera.y + camera.ry,
                  camera.z + camera.rz,
                  0.0, 1.0, 0.0)

        for mesh in world.meshes.values():
            mesh.draw()

        if window.debug:
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

        text_draw(window, camera)

    event_setup(window, camera)
    pyglet.clock.schedule_interval(camera.camera_update, 1/60.0)
