import pyglet
from pyglet.gl import *
from pyglet.window import key
from pyglet.window import mouse
from ctypes import pointer, sizeof
from math import sin, cos
import random

from camera import Menu_Camera

import primitive, shaders, fonts, game, text

def menu(window):
    window.fps_display = pyglet.clock.ClockDisplay()

    print("Generating menu batch...")
    color_cube_batch, color_cube_vl = primitive.color_cube_matrix()
    print("Done.")
    
    camera = Menu_Camera(-6.0)

    title = pyglet.text.Label('Insert Title',
                              font_size=72, font_name='Orbitron',
                              color=(255, 255, 255, 255),
                              anchor_x='center', anchor_y='center')
    splash_text = pyglet.text.Label(random.choice(text.menu),
                              font_size=20, font_name='Orbitron',
                              color=(255, 255, 0, 255),
                              anchor_x='center', anchor_y='center')
    menu = pyglet.text.Label("""F1 - Game Mode  F2 - Minecraft Loader""",
                                  font_size=20, font_name='Orbitron',
                                  color=(255,255,255,255))

    def background_draw():
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        gluLookAt(camera.x, camera.y, camera.z,
                  camera.x + camera.rx,
                  camera.y + camera.ry,
                  camera.z + camera.rz,
                  0.0, 1.0, 0.0)
        shaders.lighting.bind()
        
        for each in range(6):
            color_cube_batch.draw()
            glTranslatef(0.0, 0.0, 255/2)
            
        shaders.lighting.unbind()

    def text_draw():
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        window.fps_display.draw()

        glPushMatrix()
        glTranslatef(10, window.height - 20, 0)
        menu.draw()
        glPopMatrix()

        glTranslatef(window.width // 2, window.height * 3/4, 0.0)
        title.draw()

        glTranslatef(240, -60, 0.0)
        glRotatef(30, 0, 0, 1)
        scale = abs(sin((camera.z % 99)/3)) + 0.5
        glScalef(scale, scale, 0);
        splash_text.draw()

    @window.event
    def on_draw():
        window.clear()
        #glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        camera.perspective_projection(window)
        background_draw()

        camera.orthographic_projection(window)
        text_draw()

    @window.event
    def on_key_press(symbol, modifiers):
        if symbol == key.BACKSPACE:
            window.set_exclusive_mouse(False)
            window.exclusive = False
        # Hand control of the window over to the next driver
        elif symbol == key.F1:
            #color_cube_batch needs to be deleted
            color_cube_vl.delete()
            pyglet.clock.unschedule(camera.camera_update)
            game.game(window)
        elif symbol == key.F2:
            color_cube_vl.delete()
            pyglet.clock.unschedule(camera.camera_update)
            game.viewer(window)

    @window.event
    def on_mouse_press(x, y, button, modifiers):
        window.set_exclusive_mouse(True)
        window.exclusive = True
    
    camera.aim_camera(12, 20)
    pyglet.clock.schedule_interval(camera.camera_update, 1/60.0)
