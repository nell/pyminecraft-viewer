import shaders
from pyglet.gl import *

class Face(pyglet.graphics.Group):
    def __init__(self, normal):
        self.normal = normal
        self.parent = None

    def set_state(self):
        glNormal3f(self.normal)

    def __eq__(self, other):
        return (self.__class__ is other.__class__ and
                self.normal == other.normal)

class Texture(pyglet.graphics.Group):
    def __init__(self, texture):
        #super(Texture, self).__init__(parent=Face)
        assert texture.target == GL_TEXTURE_2D
        self.texture = texture
        self.parent = None

    def set_state(self):
        glEnable(self.texture.target)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glBindTexture(GL_TEXTURE_2D, self.texture.id)
        shaders.texture.bind()

    def unset_state(self):
        glDisable(self.texture.target)
        shaders.texture.unbind()

    def __eq__(self, other):
        return (self.__class__ is other.__class__ and
                self.texture == other.texture)


class SkyTexture(pyglet.graphics.Group):
    def __init__(self, texture, camera):
        #super(Texture, self).__init__(parent=Face)
        assert texture.target == GL_TEXTURE_2D
        self.texture = texture
        self.camera = camera
        self.parent = None

    def set_state(self):
        glDisable(GL_CULL_FACE)
        glEnable(self.texture.target)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glBindTexture(GL_TEXTURE_2D, self.texture.id)
        glTranslatef(self.camera.x, self.camera.y, self.camera.z)

    def unset_state(self):
        glEnable(GL_CULL_FACE)
        glDisable(self.texture.target)
        glTranslatef(-self.camera.x, -self.camera.y, -self.camera.z)

    def __eq__(self, other):
        return (self.__class__ is other.__class__ and
                self.texture == other.texture)

class Atlas(pyglet.graphics.Group):
    def __init__(self, parent, x=0, y=0):
        super(Atlas, self).__init__(parent=parent)
        self.x = x
        self.y = y

    def set_state(self):
        shaders.texture.uniformf('atlas', self.x, self.y)

    def __eq__(self, other):
        return (self.__class__ is other.__class__ and
                self.x == other.x and self.y == other.y)
