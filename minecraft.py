import pyglet
import zipfile, math, time
import itertools, numpy
import pymclevel
import mesh, worlds
import os
import config

from threading import Thread, Event, Lock

homedir = os.path.expanduser('~')
jar_path = '.minecraft/bin/minecraft.jar'
jar = zipfile.ZipFile(os.path.join(homedir, jar_path))
image_atlas = pyglet.image.load('terrain.png', file=jar.open('terrain.png'))
atlas = pyglet.image.ImageGrid(image_atlas,16,16)
texture_atlas = pyglet.image.TextureGrid(atlas)

# Atlas coordinates, transparency (used for mesh construction) 
texture_table = [None, # Air placeholder
                 (1.0/16.0, 15.0/16.0, False), # Stone
                 {0:(0.0/16.0, 15.0/16.0, (0.49,0.72,0.32)),
                  1:(2.0/16.0, 15.0/16.0),
                  2:(3.0/16.0, 15.0/16.0),
                  3:(3.0/16.0, 15.0/16.0),
                  4:(3.0/16.0, 15.0/16.0),
                  5:(3.0/16.0, 15.0/16.0)}, # Grass
                 (2.0/16.0, 15.0/16.0, False), # Dirt
                 (0.0/16.0, 14.0/16.0, False), # Cobblestone
                 (4.0/16.0, 15.0/16.0, False), # Wood
                 (15.0/16.0, 15.0/16.0, False), # Sapling
                 (1.0/16.0, 14.0/16.0, False), # Bedrock
                 None, # Water
                 None, # Water
                 (15.0/16.0, 0.0/16.0, False), # Lava
                 (15.0/16.0, 0.0/16.0, False), # Lava
                 (2.0/16.0, 14.0/16.0, False), # Sand
                 (3.0/16.0, 14.0/16.0, False), # Gravel
                 (0.0/16.0, 13.0/16.0, False), # Gold Ore
                 (1.0/16.0, 13.0/16.0, False), # Iron Ore
                 (2.0/16.0, 13.0/16.0, False), # Coal
                 (4.0/16.0, 14.0/16.0, False), # Logs
                 {0:(5.0/16.0, 12.0/16.0, (0.133,0.545,0.133)), 
                  1:(5.0/16.0, 12.0/16.0, (0.133,0.545,0.133)), 
                  2:(5.0/16.0, 12.0/16.0, (0.133,0.545,0.133)), 
                  3:(5.0/16.0, 12.0/16.0, (0.133,0.545,0.133)), 
                  4:(5.0/16.0, 12.0/16.0, (0.133,0.545,0.133)), 
                  5:(5.0/16.0, 12.0/16.0, (0.133,0.545,0.133))}, # Leaves
                 (0.0/16.0, 12.0/16.0, False), # Sponge
                 (1.0/16.0, 12.0/16.0, True), # Glass
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 (0.0/16.0, 11.0/16.0, False), # Wool
                 None,
                 (12.0/16.0, 15.0/16.0, True), # Yellow flower
                 (13.0/16.0, 15.0/16.0, True), # Red flower
                 (13.0/16.0, 14.0/16.0, True), # Mushroom
                 (12.0/16.0, 14.0/16.0, True), # Toadstool
                 (7.0/16.0, 13.0/16.0, False), # Gold block
                 (6.0/16.0, 13.0/16.0, False), # Iron block
                 {0:(6.0/16.0, 15.0/16.0),
                  1:(6.0/16.0, 15.0/16.0),
                  2:(5.0/16.0, 15.0/16.0),
                  3:(5.0/16.0, 15.0/16.0),
                  4:(5.0/16.0, 15.0/16.0),
                  5:(5.0/16.0, 15.0/16.0)}, # Stone block
                 {0:(6.0/16.0, 15.0/16.0),
                  1:(6.0/16.0, 15.0/16.0),
                  2:(5.0/16.0, 15.0/16.0),
                  3:(5.0/16.0, 15.0/16.0),
                  4:(5.0/16.0, 15.0/16.0),
                  5:(5.0/16.0, 15.0/16.0)}, # Stone half block
                 (7.0/16.0, 15.0/16.0, False), # Brick
                 (8.0/16.0, 15.0/16.0, False), # TNT
                 (3.0/16.0, 13.0/16.0, False), # Bookshelve
                 (4.0/16.0, 13.0/16.0, False), # Mossy cobble
                 (5.0/16.0, 13.0/16.0, False), # Obsidian
                 (0.0/16.0, 10.0/16.0, True), # Torch
                 (15.0/16.0, 14.0/16.0, True), # Fire
                 (1.0/16.0, 11.0/16.0, True), # Monster spawner
                 (4.0/16.0, 15.0/16.0, False), # Wood stair
                 {0:(9.0/16.0, 14.0/16.0),
                  1:(9.0/16.0, 14.0/16.0),
                  2:(10.0/16.0, 14.0/16.0),
                  3:(10.0/16.0, 14.0/16.0),
                  4:(10.0/16.0, 14.0/16.0),
                  5:(11.0/16.0, 14.0/16.0)}, # Chest
                 (4.0/16.0, 10.0/16.0, False), # Redstone
                 (2.0/16.0, 12.0/16.0, False), # Diamond ore
                 (8.0/16.0, 14.0/16.0, False), # Diamond
                 {0:(11.0/16.0, 13.0/16.0),
                  1:(11.0/16.0, 13.0/16.0),
                  2:(12.0/16.0, 12.0/16.0),
                  3:(12.0/16.0, 12.0/16.0),
                  4:(11.0/16.0, 12.0/16.0),
                  5:(11.0/16.0, 12.0/16.0)}, # Workbench
                 (15.0/16.0, 10.0/16.0, True), # Wheat
                 (7.0/16.0, 9.0/16.0, False), # Soil
                 (12.0/16.0, 13.0/16.0, False), # Furnace
                 (13.0/16.0, 12.0/16.0, False), # Lit Furnace
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 None,
                 (3.0/16.0, 12.0/16.0, False), # Redstone ore
                 (3.0/16.0, 12.0/16.0, False), # Glowing redstone ore
                 None,
                 None,
                 None,
                 {0:(2.0/16.0, 11.0/16.0), 
                  1:(2.0/16.0, 15.0/16.0), 
                  2:(4.0/16.0, 11.0/16.0), 
                  3:(4.0/16.0, 11.0/16.0), 
                  4:(4.0/16.0, 11.0/16.0), 
                  5:(4.0/16.0, 11.0/16.0)}, # Snow tile
                 (3.0/16.0, 11.0/16.0, True), # Ice
                 (2.0/16.0, 11.0/16.0, False), # Snow block
                 (6.0/16.0, 11.0/16.0, True), # Cactus
                 (8.0/16.0, 11.0/16.0, False), # Clay
                 (9.0/16.0, 11.0/16.0, True), # Reeds
                 (10.0/16.0, 11.0/16.0, True), # Jukebox
                 None,
                 (6.0/16.0, 8.0/16.0, False), # Pumpkin
                 (7.0/16.0, 9.0/16.0, False), # Netherstone
                 (8.0/16.0, 9.0/16.0, False), # Slow sand
                 (9.0/16.0, 9.0/16.0, False), # Lightstone
                 None,
                 (8.0/16.0, 8.0/16.0, False), # Jack-o-lantern
                 ] 

class PymclevelProxy(object):
    def __init__(self, world):
        self.level = world
        self.loaded = dict()
        self.lock = Lock()

    def getChunk(self, x, y):
        with self.lock:
            if (x,y) not in self.loaded:
                self.loaded[x,y] = self.level.getChunk(x,y)
        return self.loaded[x,y]

class MinecraftChunk(object):
    chunkSize = (16,128,16)    
    def __init__(self, world, coord):
        self.coord = coord
        self.world = world

        self.updating = Event()
        self.batching = Event()
        self.ready = Event()

        self.gl_data = []

        self.batch = pyglet.graphics.Batch()
        self.running = Thread(target=self.thread)
        self.running.daemon = True

    def get(self, x, y):
        a = self.world.getChunk(x, y).Blocks.swapaxes(1,2)
        if a != None:
            return a
        else:
            return numpy.zeros(self.chunkSize)

    def thread(self):
        """Child thread to render in."""
        while True:
            self.updating.wait()
            print("Updating chunk {0}...".format(self.coord))
            block = self.get(*self.coord)
            neighbors = {
                (-1,0,0):self.get(self.coord[0]-1,self.coord[1]),
                (1,0,0):self.get(self.coord[0]+1,self.coord[1]),
                (0,0,-1):self.get(self.coord[0],self.coord[1]-1),
                (0,0,1):self.get(self.coord[0],self.coord[1]+1)}
            # Build the chunk mesh
            vm = mesh.Voxel(block, 
                            neighbors,
                            (self.coord[0],0,self.coord[1]))
            # Pass it back to the rendering process
            if config.profile:
                import cProfile
                exec_global = {'self':self, 
                               'vm':vm, 
                               'texture_table':texture_table}
                cProfile.runctx('self.gl_data = vm.mesh(texture_table)', 
                                exec_global,
                                {}, 
                                "profile/{0}_{1}".format(*self.coord))
            else:
                self.gl_data = vm.mesh(texture_table)
            self.updating.clear()
            self.batching.set()

    def start(self):
        print("Starting chunk... {0}".format(self.coord))
        self.running.start()

    def update(self):
        """Start a new chunk update.

        This regenerates the mesh and updates the list of vertices."""
        self.ready.clear()
        self.updating.set()

    def draw(self):
        if self.batching.is_set():
            self.update_batch()
            self.batching.clear()
            self.ready.set()
        if self.ready.is_set():
            self.batch.draw()

    def update_batch(self):
        m = self.gl_data
        if hasattr(self, "vl"):
            self.vl.delete()
        self.vl = self.batch.add(len(m[0])/3,
                                 pyglet.gl.GL_TRIANGLES, None, 
                                 ('v3f', m[0]),
                                 ('n3f', m[1]),
                                 ('t4f', m[2]),
                                 ('c3f', m[3]))

class MinecraftWorld(worlds.GameWorld):
    def __init__(self, num = 1):
        super(MinecraftWorld, self).__init__()
        self.level = pymclevel.loadWorldNumber(num)
        self.level_proxy = PymclevelProxy(self.level)
        del(self.meshes['sky'])

    def load(self, chunk):
        if chunk not in self.loaded:
            self.loaded[chunk] = MinecraftChunk(self.level_proxy, chunk)
            self.loaded[chunk].start()
        return self.loaded[chunk]

    def mesh_load(self, coord = (0,0), distance = 1):
        for chunk in self.level.presentChunks:
            # Center loading around coord
            if not self.meshes.has_key(chunk) and \
                    math.sqrt((coord[0] - chunk[0])**2 + 
                              (coord[1] - chunk[1])**2) < distance:
                
                c = self.load(chunk)
                if not c.ready.is_set():
                    c.update()

    def draw(self):
        cx = self.camera.x
        crx = self.camera.rx
        cy = self.camera.y
        cry = self.camera.ry
        for k in self.loaded:
            self.loaded[k].draw()
            """
            if self.loaded[k].ready.is_set() and \
                    -35 < (cx + crx * 20) - k[0] * 16 < 35 and \
                    -35 < (cy + cry * 20) - k[0] * 16 < 35:
                
"""

if __name__ == '__main__':
    world = Minecraft_World(5)
    world.mesh_load(distance=2)
