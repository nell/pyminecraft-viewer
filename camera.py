from math import cos,sin,tan
from pyglet.gl import *

class Camera(object):
    fov = 65

    def __init__(self, z):
        # Coordinate
        self.x = 0.0
        # Rate of change
        self.dx = 0.0
        # Rotation
        self.rx = 0.0
        # Mouse
        self.mx = 90.0

        self.y = 0.0
        self.dy = 0.0
        self.ry = 0.0
        self.my = 90.0
        
        self.z = z
        self.dz = 0.0
        self.rz = 0.0

        self.fly = True

    def perspective_projection(self, window):
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(self.fov, window.width / float(window.height), 0.1, 1000)

    def orthographic_projection(self, window):
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluOrtho2D(0, window.width, 0, window.height)

    def camera_update(self, dt):
        self.x += self.dz * self.rx
        if self.fly:
            self.y += self.dz * self.ry
        self.z += self.dz * self.rz

    def aim_camera(self, dx, dy):
        self.mx = (self.mx + dx) % 360
        self.my = self.my + dy
        if self.my > 175:
            self.my = 175
        elif self.my < 5:
            self.my = 5
        rx = self.mx / 180.0 * 3.1415
        ry = self.my / 180.0 * 3.1415            

        self.rx = sin(ry) * cos(rx)
        self.ry = cos(ry)
        self.rz = sin(ry) * sin(rx)

class Menu_Camera(Camera):
    def __init__(self, z):
        # Coordinate
        self.x = 0.0
        # Rate of change
        self.dx = 0.0
        # Rotation
        self.rx = 0.0
        # Mouse
        self.mx = 90.0

        self.y = 255 / 8 + 10
        self.dy = 0.0
        self.ry = 0.0
        self.my = 90.0
        
        self.z = z
        self.dz = 0.2
        self.rz = 0.0

        self.fly = False

    def camera_update(self, dt):
        if self.z > 255/2:
            self.z = self.dz * self.rz
        else:
            self.z += self.dz * self.rz
