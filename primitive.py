import pyglet
import random
from pyglet.gl import *

cube_normals = [(0,1,0),(0,-1,0),(0,0,1),(0,0,-1),(-1,0,0),(1,0,0)]
full_cube_normals = reduce(lambda x, y: x + y, [x * 6 for x in cube_normals])
cube_n3f = reduce(lambda x, y: x+y,(x*6 for x in cube_normals))

def tex_map(ray, normal):
    x_zer, y_zer = 0, 0
    x_one, y_one = ray[0], ray[1]
    if normal == (0,1,0):
        return (x_one,y_zer,
                x_zer,y_zer,
                x_zer,y_one,
                x_one,y_zer,
                x_zer,y_one,
                x_one,y_one)
    elif normal == (0,-1,0):
        return (x_one,y_one,
                x_zer,y_one,
                x_zer,y_zer,
                x_one,y_one,
                x_zer,y_zer,
                x_one,y_zer)
    elif normal == (0,0,1):
        return (x_one,y_one,
                x_zer,y_one,
                x_zer,y_zer,
                x_one,y_one,
                x_zer,y_zer,
                x_one,y_zer)
    elif normal == (0,0,-1):
        return (x_one,y_zer,
                x_zer,y_zer,
                x_zer,y_one,
                x_one,y_zer,
                x_zer,y_one,
                x_one,y_one)
    elif normal == (1,0,0):
        # Flip the texture ray due to rotation
        x_one, y_one = y_one, x_one
        return (x_one,y_one,
                x_zer,y_one,
                x_zer,y_zer,
                x_one,y_one,
                x_zer,y_zer,
                x_one,y_zer)
    elif normal == (-1,0,0):
        # ^^
        x_one, y_one = y_one, x_one
        return (x_zer,y_one,
                x_one,y_one,
                x_one,y_zer,
                x_zer,y_one,
                x_one,y_zer,
                x_zer,y_zer)

def quad_vertices(cord, normal, ray):
    """cordinate position of corner
normal vector of face
ray length

To be rendered as GL_TRIANGLES."""
    x, y, z = cord
    xr, yr, zr = ray
    vertices = dict()
    if normal == (0,1,0):
        return (x+xr,y+yr,z,
                x,y+yr,z,
                x,y+yr,z+zr,
                x+xr,y+yr,z,
                x,y+yr,z+zr,
                x+xr,y+yr,z+zr)
    elif normal == (0,-1,0):
        return (x+xr,y,z+zr,
                x,y,z+zr,
                x,y,z,
                x+xr,y,z+zr,
                x,y,z,
                x+xr,y,z)
    elif normal == (0,0,1):
        return (x+xr,y+yr,z+zr,
                x,y+yr,z+zr,
                x,y,z+zr,
                x+xr,y+yr,z+zr,
                x,y,z+zr,
                x+xr,y,z+zr)
    elif normal == (0,0,-1):
        return (x+xr,y,z,
                x,y,z,
                x,y+yr,z,
                x+xr,y,z,
                x,y+yr,z,
                x+xr,y+yr,z)
    elif normal == (-1,0,0):
        return (x,y+yr,z+zr,
                x,y+yr,z,
                x,y,z,
                x,y+yr,z+zr,
                x,y,z,
                x,y,z+zr)
    elif normal == (1,0,0):
        return (x+xr,y+yr,z,
                x+xr,y+yr,z+zr,
                x+xr,y,z+zr,
                x+xr,y+yr,z,
                x+xr,y,z+zr,
                x+xr,y,z)

def inline_cube_vertices(vertices,x,y,z,r=1):
    for norm in cube_normals:
        vertices += quad_vertices((x,y,z), norm, (r,r,r))

def cube_vertices(x,y,z,r=1):
    vertices = list()
    inline_cube_vertices(vertices,x,y,z,r)
    return vertices

texture_map_cache = {}
def cube_tex_map(r = 1):
    if r not in texture_map_cache:
        l = [tex_map((1,1), key) for key in cube_normals]
        texture_map_cache[r] = [item for sublist in l for item in sublist]
    return texture_map_cache[r]

def cube_vl_gauss(x, y, z, sd):
    x = x + random.gauss(1,sd)
    y = y + random.gauss(1,sd)
    z = z + random.gauss(1,sd)
    return cube_vl(x,y,z)

def cube_vl(x, y, z):
    vertices = cube_vertices(x,y,z)
    v3f = (GLfloat * len(vertices))(*vertices)
    return v3f

def color_cube_matrix():
    offset = 0
    #offset = random.randint(0,255)
    i = [0,1,2]
    #random.shuffle(i)

    batch = pyglet.graphics.Batch()
    vl_list = list()
    color_list = list()
    for x in range(-255,255,22):
        for y in range(0,255,22):
            for z in range(-255,255,20):
                c = [(abs(x) + offset) % 255, y, abs(z)]
                color = (c[i[0]], c[i[1]], c[i[2]], 255)
                if y > 255 / 2:
                    vl_list += cube_vl_gauss(x/1.5, y/4+20, z/4, 6)
                else:
                    vl_list += cube_vl_gauss(x/1.5, y/4, z/4, 6)
                color_list += (color * 36)
    vertices = len(vl_list)/3
    polygons = vertices / 6 / 6 # per face (TRIANGLES) / faces
    vl = batch.add(vertices, pyglet.gl.GL_TRIANGLES, None, 
                   ('v3f',vl_list),
                   ('c4B', color_list), 
                   ('n3f', cube_n3f * polygons))
    return (batch, vl)
